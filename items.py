# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class CityimagesItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    cityName=scrapy.Field()
    imageUrl=scrapy.Field()
    lat=scrapy.Field()
    lng=scrapy.Field()
    countryName=scrapy.Field()
    date=scrapy.Field()
    locationName=scrapy.Field()
    
