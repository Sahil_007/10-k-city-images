import scrapy
import requests
import json
import functools
from cityImages.items import CityimagesItem
from bs4 import BeautifulSoup
class CityImageSpider(scrapy.Spider):
	handle_httpstatus_list = [404]
	name="cityimages"
	start_urls=["https://www.instagram.com/explore/locations/?page=1","https://www.instagram.com/explore/locations/?page=2","https://www.instagram.com/explore/locations/?page=3","https://www.instagram.com/explore/locations/?page=4","https://www.instagram.com/explore/locations/?page=5","https://www.instagram.com/explore/locations/?page=6"]


	def parse(self,response):
		request_one=requests.get(response.url)
		soup=BeautifulSoup(request_one.text,'html.parser')
		a_list=soup.findAll('script',{"type":"text/javascript"})
		location_text=a_list[2].text
		script_temp=json.loads(location_text.strip("window._sharedData = ").strip(";"))
		country_list_temp=script_temp['entry_data']['LocationsDirectoryPage'][0]['country_list']
		for each in country_list_temp:
			country_name=each['name']
			for one in range(1):
				url="https://www.instagram.com/explore/locations/"+each['id']+"/"+each['slug']+"/"+"?page="+str(one+1)
				yield scrapy.Request(url,callback=(functools.partial(self.parse_city,country_name=country_name)))

	def parse_city(self,response,country_name):
		request_second=requests.get(response.url)
		soup=BeautifulSoup(request_second.text,'html.parser')
		a_list=soup.findAll('script',{"type":"text/javascript"})
		location_text=a_list[2].text
		script_temp=json.loads(location_text.strip("window._sharedData = ").strip(";"))
		city_list_temp=script_temp['entry_data']['LocationsDirectoryPage'][0]['city_list']
		for each in city_list_temp:
			city_name=each['name']
			for one in range(1):
				url="https://www.instagram.com/explore/locations/"+each['id']+"/"+each['slug']+"/"+"?page="+str(one+1)
				yield scrapy.Request(url,callback=(functools.partial(self.parse_location,country_name=country_name,city_name=city_name)))
		
	def parse_location(self,response,country_name,city_name):
		request_third=requests.get(response.url)
		soup=BeautifulSoup(request_third.text,'html.parser')
		a_list=soup.findAll('script',{"type":"text/javascript"})
		location_text=a_list[2].text
		script_temp=json.loads(location_text.strip("window._sharedData = ").strip(";"))
		location_list_temp=script_temp['entry_data']['LocationsDirectoryPage'][0]['location_list']
		for each in location_list_temp:
			location_name=each['name']
			for one in range(1):
				url="https://www.instagram.com/explore/locations/"+each['id']+"/"+each['slug']+"/"+"?page="+str(one+1)
				yield scrapy.Request(url,callback=(functools.partial(self.parse_final,country_name=country_name,city_name=city_name,location_name=location_name)))

	def parse_final(self,response,country_name,city_name,location_name):
		request_fourth=requests.get(response.url)
		soup=BeautifulSoup(request_fourth.text,'html.parser')
		a_list=soup.findAll('script',{"type":"text/javascript"})
		image_text=a_list[2].text
		script_temp=json.loads(image_text.strip("window._sharedData = ").strip(";"))	
		image_list=script_temp['entry_data']['LocationsPage'][0]['location']['media']['nodes']
		lat=script_temp['entry_data']['LocationsPage'][0]['location']['lat']
		lng=script_temp['entry_data']['LocationsPage'][0]['location']['lng']
		
		for each in image_list:
			image_source=each['thumbnail_resources'][-1]['src']
			date=each['date']
			item=CityimagesItem()
			item['cityName']=city_name
			item['countryName']=country_name
			item['locationName']=location_name
			item['imageUrl']=image_source
			item['lat']=lat
			item['lng']=lng
			item['date']=date
			
			yield item
		
			
				
									
			
